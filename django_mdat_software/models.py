from django.db import models


class MdatSoftware(models.Model):
    id = models.BigAutoField(primary_key=True)
    app_name = models.CharField(unique=True, max_length=250)

    class Meta:
        db_table = "mdat_software"
